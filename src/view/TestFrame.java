package view;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;


public class TestFrame extends JFrame {
   public TestFrame() {
	   createFrame();
	   
   }
   public void createFrame() {
	   showProjectName = new JLabel("new project");
	   showResults = new JTextArea("your resutls will be showed here");
	   setLayout(new BorderLayout());
	   add(showProjectName, BorderLayout.NORTH);
	   add(showResults, BorderLayout.CENTER);
	   
   }
   public void setProjectName(String s) {
	   showProjectName.setText(s);
   }
   public void setResult(String str) {
       this.str = str;
	   showResults.setText(str);
	   

   }
   public void extendResult(String str) {
	   this.str = this.str+"\n"+str;
	   showResults.setText(this.str);
   }
  
   private JLabel showProjectName;
   private JTextArea showResults;
   private String str;
} 
